This code has been tested under Windows 10 version 1903 using Matlab 2019b with all toolboxes installed.
To run this code example you need to keep the folder structure provided in this repository as depicted below:

CloneClassifier
|
|_dataIn
|
|_dataOut
|
|_src

The image data folder needs to be placed into the folder dataIn

To complete the dependencies, please add the following files to the src folder:

-dirrec.m (can be downloaded from 'https://nl.mathworks.com/matlabcentral/fileexchange/15505-recursive-dir')
-xmlreadstring.m (can be downloaded from 'https://nl.mathworks.com/matlabcentral/fileexchange/46844-deployanywhere-zip')

To trigger code execution run the main.m script located in src from Matlab