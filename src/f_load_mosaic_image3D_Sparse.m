function [XYmosaic, XYmosaicCell, XPositions, YPositions, ResolutionXY] = f_load_mosaic_image3D_Sparse(files, InfoTable, row, column, channel)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
        if isfield(table2struct(InfoTable), 'row') % For compatibility with data loading before 20161201
            InfoRowFilter = (InfoTable.row == row) & (InfoTable.column == column);
        elseif isfield(table2struct(InfoTable), 'Row') % Recommended use
            InfoRowFilter = (InfoTable.Row == row) & (InfoTable.Column == column);
        end
        InfoSelection = InfoTable(InfoRowFilter, InfoTable.Properties.VariableNames);
        CoordinateStrings = [InfoSelection.PositionX, InfoSelection.PositionY];
        CoordinatesNumeric = cellfun(@(x) str2double(x), CoordinateStrings);
        InfoSelection.PositionX = CoordinatesNumeric(:,1);
        InfoSelection.PositionY = CoordinatesNumeric(:,2);

        InfoSelectionSorted = sortrows(InfoSelection, {'PositionY', 'PositionX'}, 'descend');
        
        XPositions = unique(InfoSelectionSorted.PositionX);
        YPositions = unique(InfoSelectionSorted.PositionY);
        ResolutionXY = unique(InfoSelectionSorted.ResolutionXY);
        XYmosaicCell = {};
        
        for f = 1:max(InfoSelectionSorted.field(:)) % loop over fields
            InfoThisField = InfoSelectionSorted((InfoSelectionSorted.field == f), :);
            cube = readflexcube(InfoThisField.filename{:} , 'PlaneCount', 1);
            ImageThisField = cube.data(:, :, 1, [channel:InfoThisField.ChannelCount:(InfoThisField.ChannelCount*InfoThisField.PlaneCount)]); %Hoechst
            % it(ImageThisField);
            rowThisField = find(YPositions == InfoThisField.PositionY);
            columnThisField = find(XPositions == InfoThisField.PositionX);
            XYmosaicCell{rowThisField, columnThisField} = ImageThisField;
        end
        
        EmptyCells = cellfun(@(x) isempty(x), XYmosaicCell);
        ImSize = size(XYmosaicCell{min(find(~EmptyCells))});
        XYmosaicCell(EmptyCells) = {zeros(ImSize, 'uint16')};
        XYmosaic = cell2mat(XYmosaicCell);
        % it(XYmosaic)

end


