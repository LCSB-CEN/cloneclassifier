function [info, areanames, areas, map] = flexinfoMosaic(files)
% FLEXINFO extract metadata from flex files
% [info, areaNames, plateLayout, areaMap] = FLEXFILES(wildcard) 
% extracts metadata from the flex files matching wildcard
% flexinfo is a struct with the following fields: 
% - filename: full path to the flex file
% - row, column: position of the flex file in the plate
% - area: numerical identifier for the plate area
% - areaname: name of the area from the plate layout
% areaNames is a cell array with areanames indexed by area ids
% plateLayout is a matrix with the assignment of area ids to well positions
% areaMap is a containers.Map mapping area names to area ids

%[~, files] = fileattrib(wildcard);
%info = struct([]);
%parfor i=1:length(files)
for i=1:length(files)
    info(i) = flexfileinfoMosaic(files{i});
end

[rows, columns] = flexplateinfo(info(1).filename);
areas = nan(rows, columns);

map = containers.Map();

k = 1;

areanames = cell(1);

for i=1:length(info)
    row = info(i).row;
    column = info(i).column;
    areaname = info(i).areaname;
    if isKey(map, areaname)
        area = map(areaname);
        areas(row, column) = area;
        info(i).area = area;
    else
        areas(row, column) = k;
        map(areaname) = k;
        info(i).area = k;
        areanames{k} = areaname;
        k = k+1;
    end
end

end

