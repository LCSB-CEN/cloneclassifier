function [rows, cols] = flexplateinfo(fname)

info = imfinfo(fname);
xmlstr = info(1).UnknownTags(1).Value;
xml = xmlreadstring(xmlstr);
cols = str2double(char(xml.getElementsByTagName('YSize').item(0).getTextContent()));
rows = str2double(char(xml.getElementsByTagName('XSize').item(0).getTextContent()));

end

