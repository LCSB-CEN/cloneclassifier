function [flexinfo, info] = flexfileinfoMosaic(fname)

flexinfo.filename = fname;

retries = 5;
while retries > 0, try %#ok<ALIGN>
    info = imfinfo(fname);
    retries = 0;
catch err
    retries = retries - 1;
    if retries == 0
        rethrow(err)
    end
    fprintf('Unable to open file %s, %d retries left...\n', fname, retries);
end, end

xmlstr = info(1).UnknownTags(1).Value;
xml = xmlreadstring(xmlstr);
flexinfo.areaname = char(xml.getElementsByTagName('AreaName').item(0).getTextContent());
flexinfo.barcode = char(xml.getElementsByTagName('Barcode').item(0).getTextContent());
attr = xml.getElementsByTagName('WellCoordinate').item(0).getAttributes();
flexinfo.row = str2double(char(attr.getNamedItem('Row').getNodeValue()));
flexinfo.column = str2double(char(attr.getNamedItem('Col').getNodeValue()));
flexinfo.meas = str2double(char(xml.getElementsByTagName('OperaMeasNo').item(0).getTextContent()));
flexinfo.field = str2double(char( xml.getElementsByTagName('Images').item(0).getElementsByTagName('Sublayout').item(0).getTextContent()));
flexinfo.timestamp = char(xml.getElementsByTagName('DateTime').item(0).getTextContent());
flexinfo.PositionX = char(xml.getElementsByTagName('PositionX').item(0).getTextContent());
flexinfo.PositionY = char(xml.getElementsByTagName('PositionY').item(0).getTextContent());
flexinfo.PositionZ = char(xml.getElementsByTagName('PositionZ').item(0).getTextContent());
Channels = {};
for i = 0:xml.getElementsByTagName('Image').getLength-1
    Channels{i+1} = char(xml.getElementsByTagName('Image').item(i).getElementsByTagName('CameraRef').item(0).getTextContent());
end
flexinfo.ChannelCount = length(unique(Channels));
flexinfo.PlaneCount = xml.getElementsByTagName('Image').getLength / flexinfo.ChannelCount;
flexinfo.ResolutionXY = eval(char(xml.getElementsByTagName('Image').item(i).getElementsByTagName('ImageResolutionX').item(0).getTextContent()));


end

