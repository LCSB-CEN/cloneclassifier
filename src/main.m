%% Clear Matlab workspace

clear
clc

%% User inputs per run

InPath = ['..', filesep, 'dataIn'];
SavePath = ['..', filesep, 'dataOut'];
Barcode = {'HO_20170921_Subcloning_YEL1'};
DisplayChannel = 1;

%% Initialize variables and prepare Java environment

mkdir(SavePath)
ImPlateSummary = {};
ObjectsAll = {};
PreviewCell = {};

%% Analysis

for n = 1:size(Barcode, 2)

    Barcode = Barcode{n};
    Objects = {};

    %% Document script

    SavePathThisBarcode = [SavePath, filesep, Barcode];
    AnalysisTimeStamp = datestr(now, 'yyyymmdd_HHMMSS');
    SavePath = [SavePathThisBarcode, filesep, 'Analysis_', AnalysisTimeStamp];
    mkdir(SavePathThisBarcode)
    FileNameShort = mfilename;
    newbackup = sprintf('%s_log.m',[SavePathThisBarcode, filesep, FileNameShort]);
    FileNameAndLocation = [mfilename('fullpath')];
    currentfile = strcat(FileNameAndLocation, '.m');
    copyfile(currentfile,newbackup);
    Computer = getenv('Computername');
    Version = ver();
    save([SavePathThisBarcode, filesep, 'MatlabVersionAndPC.mat'], 'Computer', 'Version')
    f_LogDependencies(mfilename, SavePathThisBarcode)

    %% Load data
    [files, info, areaNames, plateLayout, areaMap] = f_Local_Read_Files_Timeseries_Mosaic_BT2(InPath, Barcode, 1);

    %% Organize images
    % find images for a given mosaic

    InfoTable = struct2table(info);
    IDs = table([1:size(InfoTable, 1)]');
    IDs.Properties.VariableNames = {'ID'};
    InfoTable = [InfoTable, IDs];
    s = 0
    PlateImChannels = {};

        for row = unique(InfoTable.row)'

            for column = unique(InfoTable.column)'

                if sum((InfoTable.row == row) .* (InfoTable.column == column)) == 0
                    continue % Replacing try catch
                end

                s = s + 1
                ChannelCount = 3; % more channels needed?
                XYmosaicCells = cell(1,ChannelCount);

                for channel = 1:ChannelCount
                   [XYmosaic, XYmosaicCell, XPositions, YPositions, ResolutionXY] = f_load_mosaic_image3D_Sparse(files, InfoTable, row, column, channel);
                   XYmosaicCells{1,channel} = XYmosaicCell;
                   XYmosaics{1,channel} = XYmosaic;
                   PlateImChannels{row, column, channel} = imresize(XYmosaics{1, channel}, 0.075);
                end
                
                ch1Green = cell2mat(XYmosaicCells{1,1});% green
                ch2Red = cell2mat(XYmosaicCells{1,2});% red
                ch3Blue = cell2mat(XYmosaicCells{1,3});% blue
                
                %% Image analysis goes here
                [ObjectsThisWell, PreviewThisWell] = f_CloneClassifier(row, column, ch1Green, ch2Red, ch3Blue, SavePathThisBarcode, 1/0.075, Barcode);
                ObjectsAll{row, column} = ObjectsThisWell;
                PreviewCell{row, column} = PreviewThisWell;
                
            end
        end
        
        
        %% Collect Data
        
        Summary = vertcat(ObjectsAll{:});
        writetable(Summary, [SavePathThisBarcode, filesep, 'summary.csv'])
        writetable(Summary, [SavePathThisBarcode, filesep, 'summary.xlsx'])
        
        %% Plate previews
        
        PlateImChannelsEmpyIdx = cellfun(@(x) isempty(x), PlateImChannels);
        firstBestImSize = size(PlateImChannels{min(find(~PlateImChannelsEmpyIdx))});
        PlateImChannels(PlateImChannelsEmpyIdx) = {zeros(firstBestImSize, 'uint16')};
        
        NotGoodsizedCells = cellfun(@(x) ~min(size(x) == firstBestImSize), PlateImChannels, 'UniformOutput', false); % Remove images of wrong size (file damage exception)
        PlateImChannels(cell2mat(NotGoodsizedCells)) = {zeros(firstBestImSize, 'uint16')};
        
        plateDisplayImCh1  = cell2mat(PlateImChannels(:,:,1));
        plateDisplayImCh2  = cell2mat(PlateImChannels(:,:,2));
        plateDisplayImCh3  = cell2mat(PlateImChannels(:,:,3));
        
        plateGridMask = zeros(size(plateDisplayImCh1), 'uint16');
        plateRows = size(PlateImChannels, 1);
        plateCols = size(PlateImChannels, 2);
        
        plateGridMask([1:firstBestImSize(1):(firstBestImSize(1) * plateRows)],:) = 1;
        plateGridMask(:, [1:firstBestImSize(2):(firstBestImSize(2) * plateCols)],:) = 1;
        plateGridMask = imdilate(plateGridMask, strel('disk',3));
        
        PlatePreviewCh1 = imoverlay(imadjust(plateDisplayImCh1, [0 0.01], [0 1]), plateGridMask, [0 1 1]); % 520/35
        imwrite(PlatePreviewCh1, [SavePathThisBarcode, filesep, 'ch1_Green.png'])
        
        PlatePreviewCh2 = imoverlay(imadjust(plateDisplayImCh2, [0 0.01], [0 1]), plateGridMask, [0 1 1]); % 600/40
        imwrite(PlatePreviewCh2, [SavePathThisBarcode, filesep, 'ch2_Red.png'])
        
        PlatePreviewCh3 = imoverlay(imadjust(plateDisplayImCh3, [0 0.02], [0 1]), plateGridMask, [0 1 1]); % 450/50
        imwrite(PlatePreviewCh3, [SavePathThisBarcode, filesep, 'ch3_Blue.png'])
        
        %% Classification preview
        
        firstBestRGBSize = size(PreviewCell{min(find(~PlateImChannelsEmpyIdx))});
        PreviewCell(max(PlateImChannelsEmpyIdx,[],3)) = {zeros(firstBestRGBSize, 'uint8')};
        PlatePreviewClassification = cell2mat(PreviewCell);
        PlatePreviewClassification = imoverlay(PlatePreviewClassification, plateGridMask, [0 1 1]);
        imwrite(PlatePreviewClassification, [SavePathThisBarcode, filesep, 'Classes.png'])
        save([SavePathThisBarcode, filesep, 'Workspace.mat'])
        
end

