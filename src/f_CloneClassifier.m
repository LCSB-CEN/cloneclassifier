function [Objects, Preview] = f_CloneClassifier(row, col, ch1Green, ch2Red, ch3Blue, SavePath, ShrinkFactor, Barcode)
%Classify clones and create previews for a plate overview
%   Detailed explanation goes here

    %% Image analysis

    % Red pos
    RedLP = imfilter(ch2Red, fspecial('gaussian', 21, 7), 'symmetric');
    %it(ch2Red)
    %it(RedLP)
    RedPosMask = RedLP > 125;
    %it(RedPosMask)

    % Green Pos
    GreenLP = imfilter(ch1Green, fspecial('gaussian', 60, 20), 'symmetric');
    %it(GreenLP)
    GreenPosMask = GreenLP > 200;
    %it(GreenPosMask)

    SamplingZone = RedPosMask | GreenPosMask;
    %it(SamplingZone)
    
    BlueLP = imfilter(ch3Blue, fspecial('gaussian', 60, 20), 'symmetric');
    BluePosMask = BlueLP > 200;

    %% Features



    GreenVec = ch2Red(SamplingZone);
    RedVec = ch1Green(SamplingZone);
    Objects = table();
    Objects.Area = sum(SamplingZone(:));
    Objects.AreaRedPos = sum(RedPosMask(:));
    Objects.AreaGreenPos = sum(GreenPosMask(:));
    Objects.AreaBluePos = sum(BluePosMask(:));
    Objects.GreenMean = mean(GreenVec);
    Objects.RedMean = mean(RedVec);
    Objects.Barcode = Barcode;
    Objects.ROW = row;
    Objects.COL = col;


    if Objects.AreaBluePos / Objects.Area > 0.5
        Class = 'Blue';
    else
        if (Objects.AreaRedPos / Objects.Area > 0.9) & ~(Objects.AreaGreenPos / Objects.Area > 0.1) & (Objects.Area >= 2e4)
            Class = 'Red';
        elseif ~(Objects.AreaRedPos / Objects.Area > 0.1) & (Objects.AreaGreenPos / Objects.Area > 0.9) & (Objects.Area >= 2e4)
            Class = 'Green';
        elseif (Objects.AreaRedPos / Objects.Area > 0.1) & (Objects.AreaGreenPos / Objects.Area > 0.1) & (Objects.Area >= 2e4)
            Class = 'RedGreen';
        else
            Class = 'Neg';
        end
    end

    Objects.Class = {Class};

    %% Previews
    RGB = cat(3, imadjust(ch2Red, [0 0.01], [0 1]), imadjust(ch1Green, [0 0.01], [0 1]),  imadjust(ch3Blue, [0 0.02], [0 1]));
    % imtool(RGB)
    Preview = imoverlay(RGB, imdilate(bwperim(SamplingZone), strel('disk', 10)), [1 1 1]);
    Preview = insertText(Preview, [1 1], Class, 'FontSize', 200, 'BoxColor', 'black', 'TextColor', 'white');
    Preview = imresize(Preview, 1/ShrinkFactor);
    % imtool(Preview)

end

