function [files, info, areaNames, plateLayout, areaMap] = f_Local_Read_Files_Timeseries_Mosaic_BT2(hcsPath, barcode, timepoint)
%Author: Paul Antony 20141024
%Loads image data and metadata from a time series barcode from the hcs drive
%   hcsPath: Full path to the folder containing the meas folder, e.g., 'S:\OperaQEHS\OperaArchiveCol'
%   barcode: The barcode to be analyzed
%   timepoint: the counter of the MEAS folder to be analyzed
%   Example: [files, info, areaNames, plateLayout, areaMap] = f_Local_Read_Files_Timeseries('Y', 'OB 20141022_MDPD1_3Stains_2Controls', 1)

TimepointsStruct = dir([hcsPath, filesep, barcode]);
TimepointsTable = struct2table(TimepointsStruct);
TimepointsTableClean = varfun(@(x) regexp(x,'Meas'), TimepointsTable, 'InputVariables', {'name'});
TimepointsTableFilter = cellfun(@(x) ~isempty(x), TimepointsTableClean.Fun_name, 'UniformOutput', false);
TimepointsTableFilter = cell2mat(TimepointsTableFilter);
TimepointsTableClean2 = TimepointsTable(TimepointsTableFilter, TimepointsTable.Properties.VariableNames);
TimepointFolders = TimepointsTableClean2.name;
files = dirrec([hcsPath, filesep, barcode, filesep, TimepointFolders{timepoint}], '.flex');
[info, areaNames, plateLayout, areaMap] = flexinfoMosaic(files);

end




